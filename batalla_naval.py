"""Descripción de lo esperado:

- Matriz que representa el campo de batalla, compuesto por 5 filas y 5 columnas
- Coordenadas con letras (de la 'a' a la 'e') y números (del 1 al 5)
- Al ejecutarse el programa, se genera aleatoriamente la posición de 5 objetivos dentro de la matriz del campo
- El jugador tendrá x cantidad de intentos para acertar a los 5 objetivos
- El jugador tendrá que ingresar las coordenadas. Primero la letra y luego el número
- Si el jugador acierta, se sumará al contador de aciertos
- Si el contador de aciertos o el de intentos alcanza el valor máximo, el juego termina"""


import random

def campoDisparos():
    for (keys,values) in disparos.items():
                print(keys, values)


print("=============")
print("BATALLA NAVAL")
print("=============")

print(f"""\n Instrucciones:
- El juego consiste en un campo de 5 filas y 5 columnas, con un total de 25 espacios posibles.
- El objetivo principal es acertar a los 5 objetivos distribuidos aleatoriamente entre los 25 espacios.
- Los disparos se hacen eligiendo coordenadas. Primero una letra entre la 'a' y la 'e' y luego un número
entre el 1 y el 5.
- Tendrás 15 intentos. El juego termina cuando se termine esa cantidad de intentos o cuando aciertas a los 5 objetivos.
- Ojo: no es tan fácil. Puede haber filas sin objetivos, y puede haber filas con varios de ellos.

Buena suerte. \n""")

while True:
    
    campo = {"a": [" ", " ", " ", " ", " "],
            "b": [" ", " ", " ", " ", " "],
            "c": [" ", " ", " ", " ", " "],
            "d": [" ", " ", " ", " ", " "],
            "e": [" ", " ", " ", " ", " "],}

    disparos = {"a": [" ", " ", " ", " ", " "],
            "b": [" ", " ", " ", " ", " "],
            "c": [" ", " ", " ", " ", " "],
            "d": [" ", " ", " ", " ", " "],
            "e": [" ", " ", " ", " ", " "],}

    filas = ["a", "b", "c", "d", "e"]

    columnas = ["1","2", "3", "4", "5"]

    i = 0

    while i < 5:

        letra = random.choice(filas)

        num = int(random.choice(columnas))

        if not "X" in campo[letra][num - 1]:
            campo[letra][num-1] = "X"
            i += 1
            
    intentos = 15
    aciertos = 0

    while intentos != 0:
        
        campoDisparos()
        
        print(f"\n{intentos} intento(s) restante(s)\n")

        disparo_letra = input("Ingresa la letra: ")

        while not disparo_letra in filas or len(disparo_letra) != 1:
            print("Debe ser una letra entre 'a' y 'e'\n")
            disparo_letra = input("Ingresa la letra: ")


        disparo_numero = input("Ingresa el número: ")

        while not disparo_numero in columnas or len(disparo_numero) != 1:
            print("Debe ser un número entre 1 y 5\n")
            disparo_numero = input("Ingresa el número: ")


        if "X" in campo[disparo_letra][int(disparo_numero) - 1]:
            print("\n¡Acierto!\n")
            campo[disparo_letra][int(disparo_numero) - 1] = "*"
            disparos[disparo_letra][int(disparo_numero) - 1] = "*"
            aciertos += 1
            intentos -= 1
            
            if aciertos == 5:
                campoDisparos()
                    
                print("\nGanaste. Buen trabajo.")
                break
            
        elif "A" in campo[disparo_letra][int(disparo_numero) - 1] or "-" in disparos[disparo_letra][int(disparo_numero) - 1]:
            print("\nYa disparaste ahí. Prueba en otra posición...\n")
            
            
        else:
            print("\nFallaste\n")
            if not "*" in disparos[disparo_letra][int(disparo_numero) - 1]:
                disparos[disparo_letra][int(disparo_numero) - 1] = "-"
                intentos -= 1 
            
        if intentos == 0:
            print(f"PERDISTE. {aciertos} acierto(s) logrado(s). Más suerte la próxima vez.\n")
            for (keys,values) in campo.items():
                print(keys, values)
            break    

                
    print("\nPulsa (s) para volver a jugar. Cualquier otra tecla para salir")
    recomenzar = input("> ").lower()
        
    if recomenzar == "s":
        print("\nVolviendo a empezar...\n")
            
    else:
        print("\nSaliendo del programa. Hasta pronto")
        exit()
